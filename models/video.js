const mongoose = require('../db');

const schema = mongoose.Schema({
    tag:{
        type:String
    },
    title:{
        type:String,
        required:true
    },
    description:{
        type:String,
        default:"Here goes some description"
    },
    channelTitle:{
        type:String
    },
    channelId:{
        type:String
    },
    publishedAt:{
        type:String
    },
    expiryDate:{
        type:Number
    }
})

module.exports.Video = mongoose.model('video', schema);