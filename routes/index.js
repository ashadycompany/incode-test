const express = require('express');
const router = express.Router();

router.get('/videos/:tag', require('../controllers/main').checkVideoData, require('../controllers/main').getVideoData);
router.get('/videos', require('../controllers/main').videos);
router.use(require('../controllers/main').notFound);

module.exports = router;
