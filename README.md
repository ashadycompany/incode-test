# Incode Test

#What is this?

A test project.

#Requirements

NodeJS https://nodejs.org/ MongoDB https://docs.mongodb.com/manual/installation/#tutorials

#Configuration

YouTube API key is taken from process.env.YOUTUBE_API_KEY - make sure to define one before launching.
Connection string is taken from process.env.MONGO_CONNECTION_STRING. Define one in your environment or feel free to replace it in ./db/index(line 2)

#How to run

Run '>npm start' in project folder to start the server. You can also use the '> node ./bin/www' command from the main folder.
The application gets data from videos that you find by tag from /videos/:tag request.
If there is data in the DB that has not expired yet - the app will  yield you the stored data.
The /videos/ route without a parameter yields the current data that is stored in the DB.