const mongoose = require('mongoose');
const connection = process.env.MONGO_CONNECTION_STRING;


mongoose.connect(connection, { useNewUrlParser: true }).then(() => {
    console.log("Connection to database established.");
        }).catch(err =>{
            console.log(err);
    });

module.exports = mongoose;