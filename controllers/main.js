const {google} = require('googleapis');
const Video = require('../models/video').Video;
const api_key = process.env.YOUTUBE_API_KEY;

const youtube = google.youtube({
    version: 'v3',
    auth:api_key
});


module.exports.checkVideoData = async function(req, res, next){
    try{
        const dbResults = await Video.find({});
        const {tag} = req.params;

        if(dbResults.length === 0){
            next();
        }
        else{

            if(dbResults[0].expiryDate < new Date()){
                await Video.remove({});
                next();
            }
            else{

                if(dbResults[0].tag !== tag) {
                    res.json({
                        response: `Data from previous tag - ${dbResults[0].tag} - is still stored`,
                        data:dbResults
                    });
                }
                else{
                    res.json({
                        response:`Data from searching by the ${tag} tag`,
                        data:dbResults
                    });
                }

            }
        }

    } catch(e) { throw e; }
}

module.exports.getVideoData = async function(req, res, next){
    try{

        const {tag} = req.params;
        const expiryDate = new Date();

        expiryDate.setMinutes(expiryDate.getMinutes() + 15);


        const searchResults = await youtube.search.list({
            q: tag,
            maxResults: 10,
            part: 'snippet'
        });

        for (let result of searchResults.data.items){
            await Video.create({
                tag:tag,
                title: result.snippet.title,
                description: result.snippet.description,
                channelTitle: result.snippet.channelTitle,
                channelId: result.snippet.channelId,
                publishedAt: result.snippet.publishedAt,
                expiryDate:expiryDate
            });
        }

        res.json({
            response: `Fresh data from searching by the ${tag} tag`, 
            data: await Video.find({})
        });

    } catch(e) { throw e; }
}

module.exports.videos = async function(req, res, next){
    try{

        const results = await Video.find({});


        if(results.length > 0){
            res.json({response: "Stored data from Db",
                data: results
            });

            if(results[0].expiryDate < new Date()){
                await Video.remove({});
            }
        }
        else{
            res.json({
                response:"No data stored yet. Fetch some data first with a /videos/:tag request"
            });
        }

    } catch (e) { throw e; }
}

module.exports.notFound = function(req, res, next){

    res.json({
        response: "Route not found"
    });

}